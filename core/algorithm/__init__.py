import constants

__author__ = 'adiksonline'

class Algorithm:

    NormalPattern = (2, 3, 3)
    MildPattern = (2, 2, 2)

    def __init__(self, clip, pattern, spread):
        self.clip = clip
        self.pattern = Algorithm.NormalPattern if pattern == constants.PATTERN_NORMAL else Algorithm.MildPattern
        self.spread = spread

        self.image = None

    def updateImage(self, image):
        self.image = image

    def embed(self, data, byteSize):
        pass

    def extract(self, byteSize):
        pass

    def getMaxEmbedCapacity(self, h, w):
        pass