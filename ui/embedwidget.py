import os
import sys
import constants
from core.data import EmbedInfo

__author__ = 'adiksonline'

from PyQt4.QtGui import *
from PyQt4.QtCore import *

class EmbedWidget(QWidget):
    def __init__(self):
        QWidget.__init__(self)

        self.setWindowTitle("Steganomedia")
        layout = QVBoxLayout()

        label = QLabel("Select container media:")
        hl = QHBoxLayout()
        self.containerEdit = QLineEdit()
        self.containerEdit.setReadOnly(True)
        label.setBuddy(self.containerEdit)
        self.containerButton = QPushButton("...")
        hl.addWidget(self.containerEdit)
        hl.addWidget(self.containerButton)
        layout.addWidget(label)
        layout.addLayout(hl)

        self.inputList = QListWidget()
        self.inputList.setFocusPolicy(Qt.NoFocus)
        self.inputAdd = QPushButton("Add new")
        self.inputRemove = QPushButton("Remove")

        vl = QVBoxLayout()
        vl.addWidget(self.inputAdd)
        vl.addWidget(self.inputRemove)
        vl.addStretch()
        hl = QHBoxLayout()
        hl.addWidget(self.inputList)
        hl.addLayout(vl)

        groupBox = QGroupBox("Data to embed:")
        groupBox.setLayout(hl)
        layout.addWidget(groupBox)

        label = QLabel("Save output as:")
        self.outputEdit = QLineEdit()
        self.outputEdit.setReadOnly(True)
        self.outputButton = QPushButton("...")
        hl = QHBoxLayout()
        hl.addWidget(self.outputEdit)
        hl.addWidget(self.outputButton)
        layout.addWidget(label)
        layout.addLayout(hl)

        label = QLabel("Set encryption password:")
        self.passEdit = QLineEdit()
        self.passEdit.setEchoMode(QLineEdit.Password)
        label.setBuddy(self.passEdit)
        layout.addWidget(label)
        layout.addWidget(self.passEdit)

        self.proceedButton = QPushButton("Start process")
        buttonBox = QDialogButtonBox()
        buttonBox.addButton(self.proceedButton, QDialogButtonBox.ActionRole)
        layout.addWidget(buttonBox)

        self.setLayout(layout)

        self.setMinimumSize(600, 400)
        self.setBinding()

    def setBinding(self):
        self.containerButton.clicked.connect(self.pickContainer)
        self.inputAdd.clicked.connect(self.addInput)
        self.inputRemove.clicked.connect(self.removeInput)
        self.outputButton.clicked.connect(self.pickOutput)
        self.proceedButton.clicked.connect(self.performEmbed)

    def pickContainer(self):
        fileName = QFileDialog.getOpenFileName(self, "Select video container", os.environ["HOME"], "Videos(*.mp4 *"
                                                                                                  ".avi)")
        if fileName:
            self.containerEdit.setText(fileName)

    def pickInput(self):
        fileName = QFileDialog.getOpenFileName(self, "Select input file", os.environ["HOME"], "Text(*.txt);;All "
                                                                                              "files(*.*)")
        if fileName:
            self.inputEdit.setText(fileName)

    def pickOutput(self):
        fileName = QFileDialog.getSaveFileName(self, "Save output as:", os.environ["HOME"], "Video(*.avi)")
        if fileName:
            self.outputEdit.setText(fileName)

    def addInput(self):
        dialog = InputDialog(self)
        if dialog.exec_():
            t = dialog.inputGroup.checkedId()
            if t == constants.MODE_FILE:
                string = data = dialog.inputEdit.text()
                t_string = "FILE"
            else:
                data = dialog.inputArea.toPlainText()
                string = "#RawString"
                t_string = "RAW"
            item = QListWidgetItem()
            item.setData(Qt.DisplayRole, "{}:\t{}".format(t_string, string))
            item.setData(Qt.WhatsThisRole, t)
            item.setData(Qt.UserRole, data)
            self.inputList.addItem(item)

    def removeInput(self):
        current = self.inputList.currentRow()
        self.inputList.takeItem(current)

    def verifyInput(self):
        success = True
        if not self.containerEdit.text():
            QMessageBox.warning(self, "Input Validation", "You must selected a container file to embed data")
            success = False
        elif not self.inputList.count():
            QMessageBox.warning(self, "Input Validation", "Please add at least one item to embed")
            success = False
        elif not self.outputEdit.text():
            QMessageBox.warning(self, "Input Validation", "Please specify the output filename")
            success = False
        elif self.passEdit.text().length() < 10:
            QMessageBox.warning(self, "Input Validation", "Encryption key must be at least 10 digits")
            success = False

        return success

    def performEmbed(self):
        if self.verifyInput():
            info = EmbedInfo()
            info.container = str(self.containerEdit.text())
            for i in xrange(self.inputList.count()):
                item = self.inputList.item(i)
                info.inmode.append(str(item.data(Qt.WhatsThisRole).toInt())[0])
                info.indata.append(str(item.data(Qt.UserRole).toString()))
            info.outfile = str(self.outputEdit.text())
            info.key = str(self.passEdit.text())
            self.emit(SIGNAL("proceedButtonClicked"), info)


class InputDialog(QDialog):
    def __init__(self, parent):
        super(InputDialog, self).__init__(parent)

        self.inputGroup = QButtonGroup()
        self.inputGroup.setExclusive(True)
        rawButton = QRadioButton("Enter raw string")
        fileButton = QRadioButton("Select file")
        fileButton.setChecked(True)
        self.inputGroup.addButton(rawButton, constants.MODE_TEXT)
        self.inputGroup.addButton(fileButton, constants.MODE_FILE)

        layout = QVBoxLayout()
        layout.addWidget(fileButton)
        self.inputEdit = QLineEdit()
        self.inputEdit.setReadOnly(True)
        self.inputButton = QPushButton("...")
        hl = QHBoxLayout()
        hl.addWidget(self.inputEdit)
        hl.addWidget(self.inputButton)
        self.fileWidget = QWidget()
        self.fileWidget.setLayout(hl)
        layout.addWidget(self.fileWidget)

        layout.addWidget(rawButton)
        self.inputArea = QTextEdit()
        self.inputArea.setEnabled(False)
        layout.addWidget(self.inputArea)

        self.buttonBox = QDialogButtonBox()
        self.buttonBox.addButton(QDialogButtonBox.Ok)
        self.buttonBox.addButton(QDialogButtonBox.Cancel)
        layout.addWidget(self.buttonBox)
        self.setLayout(layout)

        self.setWindowTitle("Select Input Data")
        self.setMinimumWidth(500)
        self.setBinding()

    def setBinding(self):
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)
        self.connect(self.inputGroup, SIGNAL("buttonClicked (int)"), self.switchInput)
        self.inputButton.clicked.connect(self.pickInput)
        self.connect(self.inputEdit, SIGNAL("textChanged (const QString&)"), self.monitorInput)
        self.connect(self.inputGroup, SIGNAL("buttonClicked (int)"), self.monitorInput)
        self.connect(self.inputArea, SIGNAL("textChanged ()"), self.monitorInput)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

    def monitorInput(self):
        okButton = self.buttonBox.button(QDialogButtonBox.Ok)
        if self.inputGroup.checkedId() == constants.MODE_FILE:
            okButton.setEnabled(self.inputEdit.text().length() > 0)
        elif self.inputGroup.checkedId() == constants.MODE_TEXT:
            okButton.setEnabled(self.inputArea.toPlainText().length() > 0)

    def switchInput(self, button):
        self.fileWidget.setEnabled(bool(button))
        self.inputArea.setEnabled(not bool(button))

    def pickInput(self):
        fileName = QFileDialog.getOpenFileName(self, "Select input file", os.environ["HOME"], "Text(*.txt);;All "
                                                                                              "files(*.*)")
        if fileName:
            self.inputEdit.setText(fileName)



if __name__ == "__main__":
    app = QApplication(sys.argv)
    e = EmbedWidget()
    e.showMaximized()
    app.exec_()