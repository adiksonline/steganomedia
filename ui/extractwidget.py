import os
import sys
from core.data import ExtractInfo

__author__ = 'adiksonline'

from PyQt4.QtGui import *
from PyQt4.QtCore import *

class ExtractWidget(QWidget):
    def __init__(self):
        QWidget.__init__(self)
        layout = QVBoxLayout()

        label = QLabel("Select encoded media:")
        self.inputEdit = QLineEdit()
        self.inputEdit.setReadOnly(True)
        self.inputButton = QPushButton("...")
        hl = QHBoxLayout()
        hl.addWidget(self.inputEdit)
        hl.addWidget(self.inputButton)
        layout.addWidget(label)
        layout.addLayout(hl)

        label = QLabel("Enter unlock password:")
        self.passEdit = QLineEdit()
        self.passEdit.setEchoMode(QLineEdit.Password)
        label.setBuddy(self.passEdit)
        layout.addWidget(label)
        layout.addWidget(self.passEdit)

        self.proceedButton = QPushButton("Begin extraction")
        buttonBox = QDialogButtonBox()
        buttonBox.addButton(self.proceedButton, QDialogButtonBox.ActionRole)
        layout.addWidget(buttonBox)

        self.outputList = QListWidget()
        self.saveButton = QPushButton("Save all")
        buttonBox = QDialogButtonBox()
        buttonBox.addButton(self.saveButton, QDialogButtonBox.ActionRole)
        vl = QVBoxLayout()
        vl.addWidget(self.outputList)
        vl.addWidget(buttonBox)

        self.outputBox = QGroupBox("Extracted Data:")
        self.outputBox.setLayout(vl)
        self.outputBox.setEnabled(False)
        layout.addWidget(self.outputBox)

        self.setLayout(layout)
        self.setBinding()

    def setBinding(self):
        self.inputButton.clicked.connect(self.pickInput)
        self.proceedButton.clicked.connect(self.performExtraction)

    def pickInput(self):
        fileName = QFileDialog.getOpenFileName(self, "Select video container", os.environ["HOME"], "Videos(*.mp4 *"
                                                                                                  ".avi)")
        if fileName:
            self.inputEdit.setText(fileName)

    def verifyInput(self):
        success = True
        if not self.inputEdit.text():
            QMessageBox.warning(self, "Input Validation", "Please pick an input file to proceed")
            success = False
        elif self.passEdit.text().length() < 10:
            QMessageBox.warning(self, "Input Validation", "Decryption key must be at least 10 digits")
            success = False

        return success

    def performExtraction(self):
        if self.verifyInput():
            info = ExtractInfo()
            info.container = str(self.inputEdit.text())
            info.key = str(self.passEdit.text())
            self.emit(SIGNAL("proceedButtonClicked"), info)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    e = ExtractWidget()
    e.show()
    app.exec_()