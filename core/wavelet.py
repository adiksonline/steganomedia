import numpy
import numpy as np
import math

__author__ = 'adiksonline'


def fwt(data):
    temp = np.zeros(data.shape)
    h = data.shape[0] >> 1
    for i in xrange(h):
        k = i << 1
        d_k = data.item(k)
        d_k1 = data.item(k+1)
        temp.itemset(i, (d_k + d_k1) * 0.5)
        temp.itemset(i+h, (d_k - d_k1) * 0.5)
    data[...] = temp


def fwt2d(data):
    levRows, levCols = data.shape

    for i in xrange(levRows):
        row = data[i, :levCols]
        fwt(row)
        data[i, :levCols] = row

    for i in xrange(levCols):
        col = data[:levRows, i]
        fwt(col)
        data[:levRows, i] = col

    levCols >>= 1
    levRows >>= 1

    a = data[:levRows, :levCols]
    h = data[:levRows, levCols:]
    v = data[levRows:, :levCols]
    d = data[levRows:, levCols:]
    return a, (h, v, d)


def iwt(data):
    temp = np.zeros(data.shape)
    h = data.shape[0] >> 1
    for i in xrange(h):
        k = i << 1
        d_i = data.item(i)
        d_ih = data.item(i+h)
        temp.itemset(k, d_i+d_ih)
        temp.itemset(k+1, d_i-d_ih)
    data[...] = temp


def iwt2d((a, (h, v, d)),):
    l_ = np.vstack((a, v))
    h_ = np.vstack((h, d))

    data = np.hstack((l_, h_))

    levRows, levCols = data.shape 

    for i in xrange(levCols):
        col = data[:levRows, i]
        iwt(col)
        data[:levRows, i] = col

    for i in xrange(levRows):
        row = data[i, :levCols]
        iwt(row)
        data[i, :levCols] = row
    return data


if __name__ == "__main__":
    a = np.random.randint(8, 248, (8, 8))
    t = fwt2d(a.astype(np.float))
    print "Before Normalization\n"
    print t

    t = np.around(t, out=t).astype(int)
    print "\n\nAfter Normalization\n"
    print t

    #print t
    #print iwt2d(t)
    #print a