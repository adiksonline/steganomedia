import hashlib
import os
import ast
import subprocess
from PyQt4.QtCore import QFileInfo, QObject, SIGNAL
import constants
from core.data import EmbedInfo, ExtractInfo
import cv2 as opencv
import cv
from cStringIO import StringIO
from core.algorithm.dwt import Dwt

__author__ = 'adiksonline'


class Manager(QObject):
    def __init__(self):
        QObject.__init__(self)

    def embed(self, info):
        # TODO: emit progress signals at intervals
        assert isinstance(info, EmbedInfo)
        video = opencv.VideoCapture(info.container)

        fourcc = int(video.get(cv.CV_CAP_PROP_FOURCC))
        fps = video.get(cv.CV_CAP_PROP_FPS)
        f_width = int(video.get(cv.CV_CAP_PROP_FRAME_WIDTH))
        f_height = int(video.get(cv.CV_CAP_PROP_FRAME_HEIGHT))
        t_frames = int(video.get(cv.CV_CAP_PROP_FRAME_COUNT))

        self.dwt = Dwt(info.clip, info.pattern, info.spread)

        settings = self.getDataBitSize(info)
        maxEmbedBitSize = self.dwt.getMaxEmbedCapacity(f_height, f_width)
        dataBitSize = settings[constants.KEY_TOTAL_SIZE_BITS]
        if info.spread == constants.SPREAD_ENTIRE:
            if dataBitSize > maxEmbedBitSize*t_frames:
                #TODO: emit failure signal
                return
            maxPerFrame = (dataBitSize + dataBitSize%t_frames) / t_frames
        else:
            maxEmbeddable = info.spread * maxEmbedBitSize
            if dataBitSize > maxEmbeddable*t_frames:
                #TODO: emit failure signal
                return
            maxPerFrame = maxEmbeddable

        output = opencv.VideoWriter(info.outfile, fourcc, fps, (f_width, f_height))
        if not output.isOpened():
            # TODO: emit failure signal
            return
        self.emitStartSignal(dataBitSize)
        written = 0
        bitString = "{:032b}".format(len(str(settings))*8)
        data = StringIO("".join(chr(int(bitString[i*8:(i+1)*8], 2)) for i in xrange(4)))
        self.writeBytes(video, output, data, constants.SIZE_SETTINGS/8)
        written += constants.SIZE_SETTINGS
        self.emitProgressSignal(written)
        data = StringIO(constants.EMBED_BOUNDARY)
        self.writeBytes(video, output, data, constants.SIZE_BOUNDARY/8)
        written += constants.SIZE_BOUNDARY
        self.emitProgressSignal(written)
        data = StringIO(str(settings))
        self.writeBytes(video, output, data, len(str(settings)))
        s = len(str(settings)) * 8
        written += s
        self.emitProgressSignal(written)
        for size, name, mode in zip(settings[constants.KEY_SIZE_ITEMS], info.indata, info.inmode):
            data = StringIO(constants.EMBED_BOUNDARY)
            self.writeBytes(video, output, data, constants.SIZE_BOUNDARY/8)
            written += constants.SIZE_BOUNDARY
            self.emitProgressSignal(written)

            # TODO: encrypt data using the specified key
            data = StringIO(open(name, "rb").read() if mode == constants.MODE_FILE else name)
            self.writeBytes(video, output, data, sum(size))
            written += sum(size) * 8
            self.emitProgressSignal(written)

        success, image = video.read()

        while success:
            if opencv.waitKey(10) == 27 or image is None:
                break
            output.write(image)
            success, image = video.read()

        # TODO: merge the sound into the output video and write back to file
        executor = "avconv -i {} -vcodec copy -i {} -acodec copy -f {}".format(info.outfile, info.container,
                                                                          info.outfile+"_.avi")
        print executor
        #subprocess.call(executor)
        self.emitProgressSignal(dataBitSize)

    def extract(self, info):
        self.dwt = Dwt()

        assert isinstance(info, ExtractInfo)
        video = opencv.VideoCapture(info.container)

        fourcc = int(video.get(cv.CV_CAP_PROP_FOURCC))
        fps = video.get(cv.CV_CAP_PROP_FPS)
        f_width = int(video.get(cv.CV_CAP_PROP_FRAME_WIDTH))
        f_height = int(video.get(cv.CV_CAP_PROP_FRAME_HEIGHT))


        data = self.readBytes(video, constants.SIZE_SETTINGS/8)
        if data == None:
            #TODO: emit failure signal
            return
        temp = ["{:08b}".format(ord(i)) for i in data.read()]
        settingsSize = int("".join(temp), 2)
        print settingsSize
        data = self.readBytes(video, constants.SIZE_BOUNDARY/8)
        assert data == constants.EMBED_BOUNDARY
        data = self.readBytes(video, settingsSize/8)
        if data == None:
            #TODO: emit failure signal
            return
        settings = ast.literal_eval(data)

        passKey = hashlib.md5(info.key).hexdigest()
        if passKey != settings[constants.KEY_PASSKEY]:
            #TODO: emit failure signal
            return

        retrievedData = []
        for size, name in zip(settings[constants.KEY_SIZE_ITEMS], settings[constants.KEY_FILENAMES]):
            data = self.readBytes(video, constants.SIZE_BOUNDARY/8)
            assert data == constants.EMBED_BOUNDARY

            data = self.readBytes(video, sum(size))
            retrievedData.append((name, data))

        #TODO: decrypt retrieved data
        #TODO: emit success signal
        print retrievedData

    def readBytes(self, video, size):
        success = True
        read = 0
        data = StringIO()
        while success and read < size:
            l, bytes = self.dwt.extract(size-read)
            read += l
            data.write(bytes)
            if read < size:
                success, image = video.read()
                if opencv.waitKey(10) == 27 or image is None:
                    break
                else:
                    b, g, r = opencv.split(image)
                    self.dwt.updateImage((r, g, b))
        if read != size:
            return None
        data.seek(0)
        return data

    def writeBytes(self, video, output, data, size):
        success, image = True, None
        r, g, b = 0, 0, 0
        written = 0
        data.seek(0)
        while success and written < size:
            l = self.dwt.embed(data, size-written)
            written += l
            if written < size:
                if image is not None:
                    image = opencv.merge((b, g, r))
                    output.write(image)
                success, image = video.read()
                if opencv.waitKey(10) == 27 or image is None:
                    break
                else:
                    b, g, r = opencv.split(image)
                    self.dwt.updateImage([r, g, b])
        if written != size:
            return False
        return True

    def getDataBitSize(self, info):
        assert isinstance(info, EmbedInfo)
        settings = {}
        settings[constants.KEY_PASSKEY] = hashlib.md5(info.key).hexdigest()
        settings[constants.KEY_MODE_SPREAD] = info.spread
        settings[constants.KEY_PATTERN] = info.pattern
        filenames, sizes = [], []
        size = constants.SIZE_SETTINGS + 0 + (2 * constants.SIZE_BOUNDARY)
        size += (len(info.inmode) - 1) * constants.SIZE_BOUNDARY
        for mode, data in zip(info.inmode, info.indata):
            if mode == constants.MODE_TEXT:
                s = len(data)
                filenames.append(0)
                sizes.append((s, 0))
                size += s * 8
            else:
                fileInfo = QFileInfo(data)
                s = fileInfo.size()
                filenames += fileInfo.fileName()
                sizes.append((s, 0))
                size += s * 8
        settings[constants.KEY_SIZE_ITEMS] = sizes
        settings[constants.KEY_FILENAMES] = filenames
        settings[constants.KEY_TOTAL_SIZE_BITS] = size
        size += (len(str(settings)) + 2) * 8
        settings[constants.KEY_TOTAL_SIZE_BITS] = size

        return settings

    def emitProgressSignal(self, current):
        self.emit(SIGNAL("progressUpdated"), current)

    def emitStartSignal(self, maxValue):
        self.emit(SIGNAL("maxValueSet"), maxValue)
