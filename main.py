import sys
from steganomedia import Steganomedia
from ui.embedwidget import EmbedWidget

__author__ = 'adiksonline'

from PyQt4.QtGui import QApplication, QStyleFactory

app = QApplication(sys.argv)
font = app.font()
font.setPointSize(font.pointSize()+2)
app.setFont(font)
app.setStyle(QStyleFactory.create("Plastique"))
home = Steganomedia()
home.show()
app.exec_()