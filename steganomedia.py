from core.manager import Manager
from ui.embedwidget import EmbedWidget
from ui.extractwidget import ExtractWidget

__author__ = 'adiksonline'

from PyQt4.QtGui import *
from PyQt4.QtCore import *

class Steganomedia(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)

        self.tabs = QTabWidget()
        self.tabs.setFocusPolicy(Qt.NoFocus)

        self.embedWidget = EmbedWidget()
        self.extractWidget = ExtractWidget()
        self.tabs.addTab(self.embedWidget, "Data Embedding")
        self.tabs.addTab(self.extractWidget, "Data Extraction")
        self.setCentralWidget(self.tabs)

        self.setWindowTitle("Steganomedia")

        self.manager = Manager()
        self.progressDialog = QProgressDialog(self)
        self.progressDialog.setLabelText("Embedding data. Please wait...")

        self.setBinding()

    def setBinding(self):
        self.connect(self.embedWidget, SIGNAL("proceedButtonClicked"), self.doEmbed)
        self.connect(self.extractWidget, SIGNAL("proceedButtonClicked"), self.doExtract)
        self.connect(self.manager, SIGNAL("maxValueSet"), self.startProgress)
        self.connect(self.manager, SIGNAL("progressUpdated"), self.updateProgress)

    def doEmbed(self, info):
        self.manager.embed(info)
        QMessageBox.information(self, "Data Embedding", "The operation completed successfully")

    def doExtract(self, info):
        self.manager.extract(info)
        QMessageBox.information(self, "Data Extraction", "Data extraction was successful")

    def startProgress(self, maxValue):
        self.progressDialog.setRange(0, maxValue)
        self.progressDialog.open()

    def updateProgress(self, value):
        self.progressDialog.setValue(value)