import numpy as np
from cStringIO import StringIO
from core import wavelet as wave
import constants
from core.algorithm import Algorithm

__author__ = 'adiksonline'

class Dwt(Algorithm):

    def __init__(self, clip=constants.CLIP_LIGHT, pattern=constants.PATTERN_NORMAL, spread=100):
        Algorithm.__init__(self, clip, pattern, spread)
        self.index = 0
        self.imageIter = None
        self.ndIter = None

    def embed(self, data, byteSize):
        if self.image == None:
            return 0
        written = 0
        while not self.ndIter.finished and written < byteSize:
            value = self.ndIter.next()
            if self.pattern == Algorithm.NormalPattern:
                char = ord(data.read(1))
                int_bit = char >> 6
                v = value[0]
                v[...] = v + (int_bit - v%(2**2))
                int_bit = char >> 3 & 0b111
                v = value[1]
                v[...] = v + (int_bit - v%(2**3))
                int_bit = char & 0b111
                v = value[2]
                v[...] = v + (int_bit - v%(2**3))
                written += 1
                print written, "::", byteSize
        print "...exhausted"
        if written >= byteSize:
            return written
        elif self.index >= 3:
            #image has been exhausted
            return written
        else:
            self.image[self.index-1] = wave.iwt2d((self.a, (self.h, self.v, self.d)))
            self.updateIters()
            return written + self.embed(data, byteSize-written)

    def extract(self, byteSize, lst=[]):
        if self.image == None:
            return 0, ""
        read = 0
        while not self.ndIter.finished and read < byteSize:
            value = self.ndIter.next()
            if self.pattern == Algorithm.NormalPattern:
                v = value[0]
                msg = ""
                msg += "{:02b}".format(int(v%4))
                v = value[1]
                msg += "{:03b}".format(int(v%8))
                v = value[2]
                msg += "{:03b}".format(int(v%8))
                lst.append(chr(int(msg, 2)))
                read += 1

        if read >= byteSize:
            return read, "".join(lst)
        elif self.index >= 3:
            #image has been exhausted
            return read, "".join(lst)
        else:
            self.updateIters()
            r, d = self.extract(byteSize-read, lst)
            return read+r, d

    def getMaxEmbedCapacity(self, h, w):
        s = 3  # R, G, B segments
        return h * w * s * 3 / 4

    def updateImage(self, image):
        Algorithm.updateImage(self, image)
        self.index = 0
        self.updateIters()

    def updateIters(self):
        a, (h, v, d) = wave.fwt2d(self.image[self.index])
        self.index += 1
        np.around(a, out=a).astype(int)
        np.around(h, out=h).astype(int)
        np.around(v, out=v).astype(int)
        np.around(d, out=d).astype(int)
        self.a, self.h, self.v, self.d = a, h, v, d
        self.ndIter = np.nditer([self.v, self.h, self.d], op_flags=["readwrite"])


    def bitBufferToString(self, buffer):
        strings = StringIO()
        assert isinstance(buffer, StringIO())
        string = buffer.read(8)
        while len(string) == 8:
            strings.write(chr(int(string, 2)))
        strings.seek(0)
        return strings

    def stringToBit(self, string):
        n = ord(string)
        return "{:08b}".format(n)