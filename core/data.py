import constants

__author__ = 'adiksonline'

class EmbedInfo:
    def __init__(self, container=None, inmode=None, indata=None, outfile=None, key=None):
        self.container = container
        self.inmode = inmode or []
        self.indata = indata or []
        self.outfile = outfile
        self.key = key

        self.clip = constants.CLIP_LIGHT
        self.pattern = constants.PATTERN_NORMAL
        self.spread = 100


class ExtractInfo:
    def __init__(self, container=None, key=None):
        self.container = container
        self.key = key